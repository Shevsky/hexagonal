const path = require('path');
const root = path.resolve(__dirname, 'src/js');
const webpack = require('webpack');

const extract_plugin = require('mini-css-extract-plugin');
const extract = new extract_plugin({
	filename: 'css/[name].css'
});

module.exports = {
	stats: { children: false },
	mode: 'development',
	entry: {
		hexagonal: `${root}/index.js`
	},
	output: {
		filename: 'js/[name].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(less|css)$/,
				use: [
					{
						loader: extract_plugin.loader
					},
					{
						loader: 'css-loader',
						options: {
							modules: true,
							camelCase: 'dashes',
							localIdentName: 'hexagonal__[local]'
						}
					},
					'less-loader',
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							plugins: loader => [require('autoprefixer')()]
						}
					}
				]
			},
			{
				test: /\.(woff(2)?|eot|ttf)$/,
				loader: 'file-loader',
				options: {
					name: 'fonts/[name].[ext]',
					publicPath: '../'
				}
			}
		]
	},
	plugins: [
		extract,
		new webpack.ProvidePlugin({
			Promise: 'es6-promise-promise'
		})
	],
	resolve: {
		alias: {
			styles: `${root}/styles`,
			app: `${root}/app`,
			util: `${root}/util`
		}
	},
	devtool: 'cheap-module-inline-source-map'
};
