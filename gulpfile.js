const gulp = require('gulp');
const webpack = require('webpack');
const webpack_stream = require('webpack-stream');
const plumber = require('gulp-plumber');
const sequence = require('gulp-sequence');
const del = require('del');
const argv = require('yargs').argv;
const is_production = !!argv.production;
const webpack_config = require(`./webpack.${is_production ? 'build' : 'config'}`);

gulp.task('clean', function() {
	return del('./build/');
});

gulp.task('default', sequence('clean', 'dist', 'js'));

gulp.task('watch', sequence('clean', ['js:watch', 'dist:watch']));

gulp.task('js', function() {
	return gulp
		.src('./src/js/index.js')
		.pipe(plumber())
		.pipe(webpack_stream(webpack_config, webpack))
		.pipe(gulp.dest('./build/'));
});

gulp.task('js:watch', function() {
	webpack_config.watch = true;

	return gulp
		.src('./src/js/index.js')
		.pipe(plumber())
		.pipe(webpack_stream(webpack_config, webpack))
		.pipe(gulp.dest('./build/'));
});

gulp.task('dist', function() {
	return gulp
		.src(['./src/dist/**', './src/dist/**/.*'], {
			base: './src/dist/'
		})
		.pipe(plumber())
		.pipe(gulp.dest('./build/'));
});

gulp.task('dist:watch', ['dist'], function() {
	gulp.watch(['./src/dist/**', './src/dist/**/.*'], { cwd: './' }, ['dist']);
});
