import randomizer from 'random-color';

export default class Colorizer {
	colors = {};

	reset() {
		this.colors = {};
	}

	get(id) {
		if (!(id in this.colors)) {
			this.colors[id] = this.generate();
		}

		return this.colors[id];
	}

	generate() {
		return randomizer().hexString();
	}
}
