import { observable, computed, toJS } from 'mobx';
import uniqid from 'uniqid';

export default class Lattice {
	@observable
	table = [];
	@observable
	coords = {};
	@observable
	domains = [];
	@observable
	chance = null;

	generate(coords) {
		return new Promise((resolve, reject) => {
			if (!('x' in coords) || !('y' in coords) || !('z' in coords)) {
				reject();
			}

			this.coords = coords;

			const table = this.getTable();

			this.table = table;

			resolve(table);
		});
	}

	getDeepTable() {
		return this.table.slice();
	}

	getDeepCoords() {
		return Object.assign({}, this.coords);
	}

	getDeepDomains() {
		return this.domains.slice();
	}

	getDeepChance() {
		return this.chance;
	}

	getDeep() {
		const deep = {
			id: uniqid(),
			table: this.getDeepTable(),
			coords: this.getDeepCoords(),
			domains: this.getDeepDomains(),
			chance: this.getDeepChance(),
			count: this.getCount()
		};

		return deep;
	}

	random(chance) {
		const random = Math.random() * 100;

		if (random <= chance) {
			return 1;
		}

		return 0;
	}

	unrandomize() {
		this.chance = null;
	}

	randomize(chance) {
		this.chance = chance;

		return new Promise(resolve => {
			this.table.map((row, x) => {
				row.map((value, y) => {
					if (value !== null) {
						this.table[x][y] = this.random(chance);
					}
				});
			});

			resolve(this.table);
		});
	}

	calculate() {
		this.domains = [];

		return new Promise(resolve => {
			const domains = [];

			this.table.map((row, x) => {
				row.map((value, y) => {
					if (value === 1) {
						this.detect(domains, [x, y]);
					}
				});
			});

			this.correct(domains).then(resolve);
		});
	}

	detect(domains, coords) {
		let id = this.getDomainId(coords, domains);
		let domain = [];

		if (id !== null) {
			domain = this.getDomain(id, domains);
		}

		const near = this.getNearFilled(coords);

		if (!near.length) {
			domains.push([coords]);

			return domains;
		}

		near.map(near_coords => {
			const near_id = this.getDomainId(near_coords, domains);

			if (near_id !== null) {
				const near_domain = this.getDomain(near_id, domains);

				if (id !== null) {
					domains[id] = null;
				}

				domains[near_id] = this.merge(domain, near_domain, [coords, near_coords]);
			} else {
				if (id === null) {
					domains.push([coords, near_coords]);
				} else {
					domains[id] = this.merge(domain, [near_coords]);
				}
			}
		});

		return domains;
	}

	merge(target, ...merging) {
		merging.map(values => {
			values.map(coords => {
				const target_json = JSON.stringify(target);
				const coords_json = JSON.stringify(coords);

				if (!target_json.includes(coords_json)) {
					target.push(coords);
				}
			});
		});

		return target;
	}

	correct(domains) {
		return new Promise(resolve => {
			domains.map((values1, id) => {
				if (values1 !== null) {
					const domains_entries = domains.entries();

					for (let coords of values1) {
						for (let [idx, values2] of domains_entries) {
							if (id === idx || !values2) {
								continue;
							}

							if (JSON.stringify(values2).includes(JSON.stringify(coords))) {
								domains[id] = this.merge(values1, values2);
								delete domains[idx];
								break;
							}
						}
					}
				}
			});

			domains = domains.filter(value => value !== null);

			this.domains = domains;

			resolve(domains);
		});
	}

	isNear(coords1, coords2) {
		const near = this.getNear(coords1);

		return JSON.stringify(near).includes(JSON.stringify(coords2));
	}

	getCount() {
		let count = {
			all: 0,
			filled: 0
		};

		this.table.map((row, x) => {
			row.map((value, y) => {
				if (value !== null) {
					count.all++;

					if (value === 1) {
						count.filled++;
					}
				}
			});
		});

		return count;
	}

	getNearFilled(coords, exclude) {
		let near = this.getNear(coords).filter(this.nearFilledFilter);

		if (exclude !== undefined) {
			const exclude_json = JSON.stringify(coords);

			near = near.filter(near_coords => {
				const near_coords_json = JSON.stringify(near_coords);

				return !exclude_json.includes(near_coords_json);
			});
		}

		return near;
	}

	nearFilledFilter = coords => {
		const [x, y] = coords;
		const is_filled = this.table[x][y] === 1;

		return is_filled;
	};

	getNear(coords) {
		const near_y = this.getNearY(coords);
		const near_x = this.getNearX(coords);

		return [...near_x, ...near_y];
	}

	getNearY(coords) {
		const [x, y] = coords;

		const left = [x, y - 1];
		const right = [x, y + 1];

		const near_y = [];

		this.pushIfCanBeFilled(near_y, left, right);

		return near_y;
	}

	getNearX(coords) {
		const [x, y] = coords;

		const near_x = [];

		let x1,
			x2,
			y1,
			y2 = null;

		if (x < this.x) {
			// Верхние
			x1 = x - 1;
			y1 = y - 1;

			if (x + 1 < this.x) {
				x2 = x + 1;
				y2 = y + 1;
			} else {
				x2 = x + 1;
				y2 = y - 1;
			}
		} else if (x >= this.height - this.x) {
			// Нижние
			x1 = x + 1;
			y1 = y - 1;

			if (x - 1 >= this.height - this.x) {
				x2 = x - 1;
				y2 = y + 1;
			} else {
				x2 = x - 1;
				y2 = y + 1;
			}
		} else {
			// Промежуточные
			x1 = x + 1;
			x2 = x - 1;
			y1 = y - 1;
			y2 = y + 1;
		}

		const near_1 = [x1, y];
		const near_2 = [x2, y];
		const near_3 = [x1, y1];
		const near_4 = [x2, y2];

		this.pushIfCanBeFilled(near_x, near_1, near_2, near_3, near_4);

		return near_x;
	}

	getDomain(x, y) {
		let domains;
		if (Array.isArray(y)) {
			domains = y;
			y = undefined;
		} else {
			domains = this.domains;
		}

		let domain_id;

		if (!Array.isArray(x) && y === undefined) {
			domain_id = x;
		} else {
			domain_id = this.getDomainId(x, y);
		}

		if (domain_id in domains) {
			return domains[domain_id];
		} else {
			throw `Домен "${domain_id}" не найден`;
		}
	}

	getDomainId(x, y) {
		let domains;
		if (Array.isArray(y)) {
			domains = y;
		} else {
			domains = this.domains;
		}

		if (!domains.length) {
			return null;
		}

		if (Array.isArray(x)) {
			[x, y] = x;
		}

		const value = this.table[x][y];

		const is_filled = value === 1;

		if (!is_filled) {
			return null;
		}

		const coords_json = JSON.stringify([x, y]);
		const domains_json = JSON.stringify(domains);

		const is_in_domain = domains_json.includes(coords_json);
		if (is_in_domain) {
			let key = null;
			const domains_entries = domains.entries();

			domains: for (const [id, values] of domains_entries) {
				if (values === null) {
					continue;
				}

				for (let coords of values) {
					const domain_coords_json = JSON.stringify(coords);

					if (domain_coords_json === coords_json) {
						key = id;
						break domains;
					}
				}
			}

			return key;
		}

		return null;
	}

	get x() {
		return this.coords.x || null;
	}

	get y() {
		return this.coords.y || null;
	}

	get z() {
		return this.coords.z || null;
	}

	@computed
	get height() {
		const height = this.x + this.y - 1;

		return height;
	}

	@computed
	get width() {
		const width = this.z + (this.x - 1) + (this.y - 1);

		return width;
	}

	getTable() {
		let table = [];

		for (let n = 0; n < this.height; n++) {
			table[n] = [];

			for (let m = 0; m < this.width; m++) {
				const is_can_be_filled = this.isCanBeFilled(n, m);

				if (is_can_be_filled) {
					table[n][m] = 0;
				} else {
					table[n][m] = null;
				}
			}
		}

		return table;
	}

	pushIfCanBeFilled(array, ...array_coords) {
		array_coords.map(coords => {
			if (this.isCanBeFilled(coords)) {
				array.push(coords);
			}
		});
	}

	isCanBeFilled(x, y) {
		if (Array.isArray(x)) {
			[x, y] = x;
		}

		if (x < 0 || y < 0 || x === null || y === null || x >= this.height || y >= this.width) {
			return false;
		}

		if (x < this.x) {
			if (y < this.z + x) {
				return true;
			}
		}

		if (x > this.height - this.y - 1) {
			const max = this.z + (this.x - 1);

			if (y < max) {
				if (y < this.z + this.height - x - 1) {
					return true;
				}
			}
		}

		return false;
	}
}
