import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Button, CssBaseline, Paper, Typography } from '@material-ui/core';
import 'roboto-fontface';
import Styles from './App.less';
import Startup from 'app/components/Startup';
import Workspace from 'app/components/Workspace';

const MODE_STARTUP = 'startup';
const MODE_WORKSPACE = 'workspace';

@observer
export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			mode: MODE_STARTUP
		};
	}

	render() {
		const { lattice } = this.props;
		const { table } = lattice;

		return (
			<>
				<CssBaseline />

				<Paper className={Styles.paper}>
					<Typography variant="h5" component="h3">
						Гексагональная решетка
					</Typography>

					{this.mode !== MODE_STARTUP && (
						<Button
							variant="outlined"
							color="primary"
							className={Styles.startup__button}
							onClick={this.handleClickStartupButton}
						>
							Создать новую решетку
						</Button>
					)}

					{this.content}
				</Paper>
			</>
		);
	}

	get mode() {
		return this.state.mode;
	}

	set mode(value) {
		this.setState({
			mode: value
		});
	}

	get content() {
		switch (this.mode) {
			case MODE_STARTUP:
				return <Startup {...this.props} onClickGenerateButton={this.handleClickGenerateButton} />;
			case MODE_WORKSPACE:
				return <Workspace {...this.props} />;
		}
	}

	componentWillMount() {
		/**
		 * @param {Element} root
		 */
		const { root } = this.props;

		root.classList.add(Styles.root);
	}

	handleClickStartupButton = () => {
		this.mode = MODE_STARTUP;
	};

	handleClickGenerateButton = coords => {
		const { lattice } = this.props;

		lattice.generate(coords).then(this.handleGenerateTable);
	};

	handleGenerateTable = () => {
		this.mode = MODE_WORKSPACE;
	};
}
