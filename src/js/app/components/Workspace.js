/**
 * Большинство функционала в этом файле служит для вывода решетки на экран
 */

import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Styles from './Workspace.less';
import ClassNames from 'classnames';
import {
	TextField,
	MenuItem,
	Grid,
	Button,
	Typography,
	Table,
	TableHead,
	TableBody,
	TableCell,
	TableRow
} from '@material-ui/core';
import { Slider } from '@material-ui/lab';
import Colorizer from 'util/Colorizer';

const SCALE_FACTOR = Math.tan((30 * Math.PI) / 180);
const BLOCK_SIZES = [15, 30, 45, 60];

@observer
export default class Workspace extends Component {
	colorizer;

	constructor(props) {
		super(props);

		const size = this.getDefaultSize();

		this.colorizer = new Colorizer();
		this.state = {
			row_height: size,
			cell_width: size,
			is_entering_auto: false,
			auto_chance: 50,
			history: []
		};
	}

	render() {
		const { table, domains } = this.lattice;

		return (
			<div className={Styles.workspace}>
				<TextField
					select
					className={Styles.workspace__blockSizeSelect}
					label="Размер блоков"
					value={this.row_height}
					onChange={this.handleChangeBlockSize}
					margin="normal"
					variant="outlined"
				>
					{BLOCK_SIZES.map((option, idx) => (
						<MenuItem key={idx} value={option}>
							{option}
						</MenuItem>
					))}
				</TextField>
				<div className={Styles.workspace__box}>
					<div className={Styles.workspace__rows} style={{ marginLeft: this.workspace_row_margin }}>
						{table.map((row, n) => {
							const styles = this.getSelectedRowStyles(n);

							return (
								<div key={n} className={Styles.workspace__row} style={styles}>
									{row.map((value, m) => {
										if (value !== null) {
											const domain = this.getDomainId(n, m);

											return (
												<Cell
													colorizer={this.colorizer}
													cell_width={this.cell_width}
													row_height={this.row_height}
													row={n}
													cell={m}
													key={m}
													value={value}
													domain={domain}
													onChange={this.handleChangeCellValue}
												/>
											);
										}
									})}
								</div>
							);
						})}
					</div>
				</div>
				<Grid container spacing={24} className={Styles.workspace__infoBox}>
					<Grid item xs={12} sm={6} className={Styles.workspace__infoLeftBlock}>
						<strong>Размер решетки:</strong>
						{` `}[ {this.lattice.x}, {this.lattice.y}, {this.lattice.z} ]
					</Grid>
					{this.lattice.domains.length > 0 && (
						<Grid item xs={12} sm={6} className={Styles.workspace__infoRightBlock}>
							<strong>Количество доменов:</strong>
							{` `}
							{this.lattice.domains.length}
						</Grid>
					)}
				</Grid>
				<Grid container spacing={24} className={Styles.workspace__actionsBox}>
					<Grid item xs={12} sm={6}>
						<Button
							variant="contained"
							color="primary"
							className={Styles.workspace__actionButton}
							onClick={this.handleClickCalculateDomainsButton}
						>
							Посчитать домены
						</Button>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Button
							variant="contained"
							color="secondary"
							className={Styles.workspace__actionButton}
							onClick={this.handleClickAutoToggleButton}
						>
							Автозаполнение
						</Button>
					</Grid>
				</Grid>
				{this.is_entering_auto && (
					<div className={Styles.workspace__autoBox}>
						<Typography component="p">
							Вероятность значения "1" в решетке: <strong>{this.auto_chance_percents}</strong>
						</Typography>

						<Slider
							classes={{ container: Styles.workspace__autoSlider }}
							value={this.auto_chance}
							onChange={this.handleChangeAutoChance}
							min={1}
							max={99}
							step={1}
						/>

						<Button
							variant="contained"
							color="secondary"
							className={Styles.workspace__autoButton}
							onClick={this.handleClickAutoButton}
						>
							Заполнить с вероятностью {this.auto_chance_percents}
						</Button>
					</div>
				)}
				{this.history.length > 0 && (
					<div className={Styles.workspace__historyBox}>
						<Table>
							<TableHead>
								<TableRow>
									<TableCell>Вероятность</TableCell>
									<TableCell>Количество доменов</TableCell>
									<TableCell>Количество ячеек со значением "1"</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.history.map(({ id, table, coords, domains, chance, count }, idx) => (
									<TableRow key={id}>
										<TableCell>{chance}%</TableCell>
										<TableCell>{domains.length}</TableCell>
										<TableCell>
											<strong>{count.filled}</strong>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>
				)}
			</div>
		);
	}

	get is_entering_auto() {
		return this.state.is_entering_auto;
	}

	set is_entering_auto(value) {
		this.setState({
			is_entering_auto: value
		});
	}

	get auto_chance_percents() {
		return `${this.auto_chance}%`;
	}

	get auto_chance() {
		return this.state.auto_chance;
	}

	set auto_chance(value) {
		value = parseInt(value);

		if (isNaN(value)) {
			value = 0;
		}

		this.setState({
			auto_chance: value
		});
	}

	get history() {
		return this.state.history;
	}

	set history(value) {
		this.setState({
			history: value
		});
	}

	get row_height() {
		return this.state.row_height;
	}

	get cell_width() {
		return this.state.cell_width;
	}

	get cell_width_d2() {
		return this.cell_width / 2;
	}

	get cell_width_d3() {
		return this.cell_width / 3;
	}

	get cell_width_d12() {
		return this.cell_width / 12;
	}

	set row_height(value) {
		return this.setState({
			row_height: value
		});
	}

	set cell_width(value) {
		return this.setState({
			cell_width: value
		});
	}

	get lattice() {
		return this.props.lattice;
	}

	get workspace_row_margin() {
		const { x } = this.lattice;

		return this.cell_width_d2 * (x - 1) + this.cell_width_d3 + 'px';
	}

	getDefaultSize() {
		const { lattice } = this.props;
		const { x, y, z } = lattice;

		const sum = x + y;

		if (z <= 10 && sum <= 5) {
			return 60;
		}

		if (z <= 15 && sum <= 7) {
			return 45;
		}

		if (z <= 20 && sum <= 9) {
			return 30;
		}

		return 15;
	}

	getSelectedRowStyles(n) {
		const margin = this.getSelectedRowMargin(n);

		let styles = {
			height: this.row_height,
			marginLeft: margin
		};

		return styles;
	}

	getDomainId(n, m) {
		return this.lattice.getDomainId(n, m);
	}

	getSelectedRowMargin(n) {
		const { x } = this.lattice;

		let margin = 0;

		if (n < x) {
			let additional = (this.cell_width / 12) * n; // Для отступов между блоками

			margin = -1 * (this.cell_width_d2 * n + additional);
		} else if (n >= x) {
			let i = n - x;
			let additional = this.cell_width_d12 * x - this.cell_width_d12 * (i + 2);

			margin = -1 * this.cell_width_d2 * (x - 2) + this.cell_width_d2 * i - additional;
		}

		return `${margin}px`;
	}

	calculate() {
		return new Promise(resolve => {
			this.colorizer.reset();
			this.lattice.calculate().then(resolve);
		});
	}

	handleChangeCellValue = (data, value) => {
		const { cell, row } = data;
		const { table } = this.lattice;

		table[row][cell] = value;
		this.lattice.unrandomize();
	};

	handleChangeBlockSize = e => {
		const { value } = e.target;

		this.row_height = value;
		this.cell_width = value;
	};

	handleClickCalculateDomainsButton = () => {
		this.calculate();
	};

	handleClickAutoToggleButton = () => {
		this.is_entering_auto = !this.is_entering_auto;
	};

	handleClickAutoButton = () => {
		if (this.is_entering_auto) {
			this.lattice.randomize(this.auto_chance).then(this.handleRandomize);
		}
	};

	handleRandomize = () => {
		this.calculate().then(this.handleRandomizeCalculate);
	};

	handleRandomizeCalculate = () => {
		const deep = this.lattice.getDeep();

		this.history = [deep, ...this.history].slice(0, 10);
	};

	handleChangeAutoChance = (e, value) => {
		this.auto_chance = value;
	};
}

class Cell extends Component {
	render() {
		const { row, cell, domain } = this.props;
		// {this.is_filled && 1}
		// [{row},{cell}]
		return (
			<div className={this.classnames} style={this.cell_styles} onClick={this.handleClickCell}>
				<div className={Styles.workspace__cellTop} style={this.cell_top_styles} />
				{this.is_filled && 1}
				<div className={Styles.workspace__cellBottom} style={this.cell_bottom_styles} />
			</div>
		);
	}

	get colorizer() {
		return this.props.colorizer;
	}

	get domain() {
		return this.props.domain;
	}

	get row_height() {
		return this.props.row_height;
	}

	get cell_width() {
		return this.props.cell_width;
	}

	get cell_styles() {
		let styles = {};

		styles.width = this.cell_width;
		styles.height = this.row_height * SCALE_FACTOR;
		styles.marginTop = `${(this.row_height * SCALE_FACTOR) / 2}px`;
		styles.marginBottom = `${(this.row_height * SCALE_FACTOR) / 2}px`;
		styles.marginLeft = `${this.cell_width / 6}px`;

		if (this.domain !== null) {
			styles.backgroundColor = this.colorizer.get(this.domain);
		}

		return styles;
	}

	get cell_top_styles() {
		let styles = {};

		styles.borderLeft = `${this.cell_width / 2}px solid transparent`;
		styles.borderRight = `${this.cell_width / 2}px solid transparent`;
		styles.borderBottomWidth = `${(this.row_height * SCALE_FACTOR) / 2}px`;

		if (this.domain !== null) {
			styles.borderBottomColor = this.colorizer.get(this.domain);
		}

		return styles;
	}

	get cell_bottom_styles() {
		let styles = {};

		styles.borderLeft = `${this.cell_width / 2}px solid transparent`;
		styles.borderRight = `${this.cell_width / 2}px solid transparent`;
		styles.borderTopWidth = `${(this.row_height * SCALE_FACTOR) / 2}px`;

		if (this.domain !== null) {
			styles.borderTopColor = this.colorizer.get(this.domain);
		}

		return styles;
	}

	get classnames() {
		return ClassNames({
			[Styles.workspace__cell]: true,
			[Styles.workspace__cellFilled]: this.is_filled
		});
	}

	get is_filled() {
		const { value } = this.props;

		return value === 1;
	}

	handleClickCell = () => {
		const { value, onChange, cell, row } = this.props;

		onChange(
			{
				cell: cell,
				row: row
			},
			value === 0 ? 1 : 0
		);
	};
}
