import React, { Component, Fragment } from 'react';
import { Typography, TextField, Button } from '@material-ui/core';
import Styles from './Startup.less';

const COORDS_TYPES = [
	{
		id: 'x',
		title: 'X'
	},
	{
		id: 'y',
		title: 'Y'
	},
	{
		id: 'z',
		title: 'Z'
	}
];

const MIN_LENGTH = 1;
const MAX_LENGTH = 20;

export default class Startup extends Component {
	nodes = {};
	error = {};

	constructor(props) {
		super(props);

		this.state = {
			error: {}
		};
		this.prepareNodes();
	}

	render() {
		const { error } = this.state;
		const coords_types_length = COORDS_TYPES.length;

		return (
			<>
				<Typography component="p">Укажите размер области гексагональной решетки (X;Y;Z)</Typography>

				<div className={Styles.coords__box}>
					{COORDS_TYPES.map(({ id, title }, index) => (
						<Fragment key={index}>
							<div className={Styles.coords__item}>
								<Field
									label={title}
									nodeRef={ref => (this.nodes[id].node = ref)}
									inputRef={ref => (this.nodes[id].input = ref)}
									error={error[id]}
								/>
							</div>

							{index + 1 !== coords_types_length && <div className={Styles.coords__delimiter} />}
						</Fragment>
					))}
				</div>
				<div className={Styles.generate__box}>
					<Button
						variant="contained"
						color="primary"
						className={Styles.generate__button}
						onClick={this.handleClickGenerateButton}
					>
						Генерировать
					</Button>
				</div>
			</>
		);
	}

	prepareNodes() {
		COORDS_TYPES.map(({ id }) => {
			this.nodes[id] = {};
		});
	}

	getNode(id, type = 'node') {
		if (id in this.nodes) {
			if (type in this.nodes[id]) {
				return this.nodes[id][type];
			}
		}

		return null;
	}

	getInputNode(id) {
		return this.getNode(id, 'input');
	}

	removeInputErrors() {
		return new Promise(resolve => {
			this.setState(
				{
					error: {}
				},
				resolve
			);
		});
	}

	setInputError(id, message) {
		this.setState({
			error: {
				...this.state.error,
				[id]: message
			}
		});
	}

	getCoords() {
		const coords = {};

		COORDS_TYPES.map(({ id, title }) => {
			const node = this.getInputNode(id);
			const value = parseInt(node.value);

			if (isNaN(value)) {
				throw {
					message: 'Заполните поле',
					id: id
				};
			}

			if (value < MIN_LENGTH || value > MAX_LENGTH) {
				throw {
					message: `От ${MIN_LENGTH} до ${MAX_LENGTH}`,
					id: id
				};
			}

			coords[id] = value;
		});

		return coords;
	}

	handleClickGenerateButton = e => {
		this.removeInputErrors().then(() => {
			let coords;

			try {
				coords = this.getCoords();
			} catch (e) {
				let message = e;
				if ('message' in e) {
					message = e.message;
				}

				console.log(message);

				if ('id' in e) {
					const { id } = e;

					this.setInputError(id, message);

					const node = this.getInputNode(id);

					if (node) {
						node.focus();
					}
				}

				return false;
			}

			const { onClickGenerateButton } = this.props;

			if (typeof onClickGenerateButton === 'function') {
				this::onClickGenerateButton(coords);
			}
		});
	};
}

class Field extends Component {
	render() {
		const { nodeRef, error, ...props } = this.props;

		return (
			<TextField
				{...props}
				error={!!error}
				helperText={!!error ? error : ' '}
				ref={nodeRef}
				className={Styles.coords__itemInput}
				type="number"
				InputLabelProps={{
					shrink: true
				}}
				margin="normal"
				variant="filled"
				inputProps={{
					min: MIN_LENGTH,
					max: MAX_LENGTH
				}}
			/>
		);
	}
}
