import ReactDOM from 'react-dom';
import React from 'react';
import App from 'app/App';
import Lattice from 'app/Lattice';

window.Hexagonal = selector => {
	const root = document.querySelector(selector);
	const lattice = new Lattice();

	ReactDOM.render(<App root={root} lattice={lattice} />, root);
};
