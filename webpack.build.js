const merge = require('webpack-merge');
const common = require('./webpack.config');
const uglify = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
	performance: { hints: false },
	mode: 'production',
	plugins: [
		new uglify({
			uglifyOptions: {
				compress: {
					unsafe: true
				},
				output: {
					beautify: false,
					comments: false
				}
			}
		})
	],
	devtool: false
});
